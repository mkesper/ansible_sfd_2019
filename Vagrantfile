# -*- mode: ruby -*-
# vi: set ft=ruby :

boxes = [
  {
    :name => "buildjenkins-01",
    :ip => "192.168.122.214",
    :mem => "3072",
    :cpu => "1",
    :group => 'buildjenkins',
    :env_group => 'test1_buildjenkins',
    :vagrant_group => 'vagrant',
    :application => "buildjenkins",
    :lxc_bridge => "vbrbjk01",
  }
]

Vagrant.configure("2") do |config|
  vagrant_domain = 'example.com'

  boxes.each do |opts|
    config.vm.define opts[:name] do |config|
      # lxc needed setup
      config.vm.box = "mistermiles/debianLSB"
      config.vm.box_version = "2"
      config.vm.network :private_network, ip: opts[:ip], lxc__bridge_name: opts[:lxc_bridge]

      config.vm.hostname = opts[:name]

      # lxc
      config.vm.provider :lxc do |lxc|
        lxc.container_name = opts[:name]
        lxc.customize 'cgroup.memory.limit_in_bytes', opts[:mem]+'M'
      end

      config.vm.provision :ansible do |ansible|
        ansible.playbook = opts[:play] ||="playbook.yml"
        ansible.groups = {
          opts[:group] => [opts[:name]],
	  opts[:env_group] => [opts[:name]],
          opts[:vagrant_group] => [opts[:name]],
        }
        ansible.extra_vars = {
          application: opts[:application]
        }

        # Don't remove the host_vars because they are needed because of a
        # parsing bug of ansible_spec
        ansible.host_vars = {
          opts[:name] => { 'ansible_connection' => 'ssh',
          }
        }

        ansible.become = true
        ansible.limit = "all"
        ansible.verbose = ENV['ANSIBLE_VERBOSE'] ||= "v"
        ansible.tags = ENV['ANSIBLE_TAGS'] ||= "all"
      end
    end
  end
end
