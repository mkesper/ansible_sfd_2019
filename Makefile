DEBUG_CONTAINER?=FALSE
VAGRANT_DEFAULT_PROVIDER?=lxc
PLAYBOOK?=playbook.yml
ENVIRONMENT?=test1
PROFILE?=buildjenkins
ENV_PROFILE=$(ENVIRONMENT)_$(PROFILE)
PASSWORD_STORE_DIR=./password-store/secrets

verify: get_deps syntax-checks create ansible_spec destroy

run_check_environment: check_environment run_environment

check_environment:
	ansible-playbook --check --diff -i inventory/$(ENVIRONMENT) $(PLAYBOOK)

run_environment:
	ansible-playbook --check --diff -i inventory/$(ENVIRONMENT) $(PLAYBOOK)

run_check_profile: check_profile run_profile

check_profile:
	ansible-playbook --check --diff -i inventory/$(ENV_PROFILE) $(PLAYBOOK)

run_profile:
	ansible-playbook --check --diff -i inventory/$(ENV_PROFILE) $(PLAYBOOK)

get_deps:
	git submodule update --init
	bundle install

# Vagrant up / vagrant provision will run the playbook
# configured in the Vagrantfile: playbook.yml
create: destroy
	vagrant up

destroy:
	vagrant destroy -f

provision:
	vagrant provision

tests: syntax-checks ansible_spec

ansible_spec:
	# some services need time to start up correctly
	# before it makes sense to check them
	sleep 15
	# remove ssh host keys for local containers to prevent mismatches
	sed '/^\[192.168/d' -i ~/.ssh/known_hosts
	bundler exec rake all

syntax-checks: check-yaml check-jinja ansible_lint

ansible_lint:
	ansible-lint $(PLAYBOOK) \
	|| echo Please fix these warnings!

check-jinja:
	  for f in $$(find . -maxdepth 2 -regextype posix-egrep -type f -regex '.*j2'); do \
    /usr/bin/env python -c "import jinja2; \
      e=jinja2.Environment(loader=jinja2.FileSystemLoader('./')); \
      e.filters['ipaddr'] = func=lambda:None; \
      e.get_template('$$f')"; \
    if [ $$? -ne 0 ]; then \
      echo "Syntax Error in file: $$f."; \
      exit 1; \
    fi; \
  done

check-yaml:
	  for f in $$(find . -maxdepth 2 -name '*.yml'); do \
    /usr/bin/env python -c "import yaml; yaml.safe_load(open('$$f'))"; \
    if [ $$? -ne 0 ]; then \
      echo "Syntax Error in file: $$f."; \
      exit 1; \
    fi; \
  done
