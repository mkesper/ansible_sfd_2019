
Ansible Beispiel
----------------

Dieses Repository ist ein Beispiel, wie man ein Projekt in
Ansible strukturieren könnte, ohne Anspruch auf Vollständigkeit.

Es wird ein Jenkins installiert.
Bei Ausführung von 'make' wird dabei ein LXC-Container aus dem
Internet geladen und die Installation wird dort ausgeführt.
